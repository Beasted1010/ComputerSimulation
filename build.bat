

@echo off

cd obj

gcc -c -Wall -I ../inc ../src/main.c ../src/gates.c ../src/test_environment.c

cd ../bin

gcc -o run ../obj/main.o ../obj/gates.o ../obj/test_environment.o

cd ..
