
#include "stdio.h"
#include "stdlib.h"

#include "gates.h"


#include "test_environment.h"

int main(int argc, char** argv)
{
    if(RunTests() == FAILED)
    {
        printf("Not all tests passed!\n");
        exit(1);
    }


    return 0;
}
