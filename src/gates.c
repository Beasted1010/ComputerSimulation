
#include "gates.h"
#include <stdio.h>

/* TRUTH TABLE
    x   y   f
    1   1   1
    1   0   0
    0   1   0
    0   0   0
*/
Bit AndGate(Bit x, Bit y)
{
    if( x == 1 )
        if( y == 1 )
            return HIGH;

    if( x == 0 || y == 0 )
        return LOW;
    else
    {
        printf("One or mroe of the bits have an invalid value: x=%i, y=%i!\n", x, y);
        return INVALID;
    }
}

/* TRUTH TABLE
    x   y   f
    1   1   1
    1   0   1
    0   1   1
    0   0   0
*/
Bit OrGate(Bit x, Bit y)
{
    if( x == 1 )
        return HIGH;
    else if( y == 1 )
        return HIGH;
    else if( x == 0 && y == 0 )
        return LOW;
    else
    {
        printf("One or mroe of the bits have an invalid value: x=%i, y=%i!\n", x, y);
        return INVALID;
    }
}

/* TRUTH TABLE
    x   f
    1   0
    0   1
*/
Bit NotGate(Bit x)
{
    if( x == 1 )
        return LOW;
    else if( x == 0 )
        return HIGH;
    else
    {
        printf("The bit is an invalid value of %i!\n", x);
        return INVALID;
    }
}

/* TRUTH TABLE
    x   y   f
    1   1   0
    1   0   1
    0   1   1
    0   0   0
*/
Bit XorGate(Bit x, Bit y)
{
    if( x == 1 )
    {
        if( y == 1 )
            return LOW;
        else if( y == 0 )
            return HIGH;
    }
    else if( y == 1 )
        return HIGH;
    else if( x == 0 && y == 0 )
        return LOW;

    printf("One or mroe of the bits have an invalid value: x=%i, y=%i!\n", x, y);
    return INVALID;
}

/* TRUTH TABLE
    x   y   f
    1   1   1
    1   0   0
    0   1   0
    0   0   1
*/
Bit XnorGate(Bit x, Bit y)
{
    if( x == 1 )
    {
        if( y == 1 )
            return HIGH;
        else if( y == 0 )
            return LOW;
    }
    else if( x == 0 )
    {
        if( y == 0 )
            return HIGH;
        else if( y == 1 )
            return LOW;
    }

    printf("One or mroe of the bits have an invalid value: x=%i, y=%i!\n", x, y);
    return INVALID;
}
