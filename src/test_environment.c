
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"

#include "test_environment.h"



TestResult TestAndGate()
{
    uint8_t test_failed = 0;
    uint8_t result;

    uint8_t x, y;

    x = y = 1;
    if( !(result = AndGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i AND %i\n returned %i\n", x, y, result);
    }

    x = 1;
    y = 0;
    if( (result = AndGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i AND %i\n returned %i\n", x, y, result);
    }

    x = 0;
    y = 1;
    if( (result = AndGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i AND %i\n returned %i\n", x, y, result);
    }

    x = y = 0;
    if( (result = AndGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i AND %i\n returned %i\n", x, y, result);
    }

    if( test_failed )
        return FAILED;

    return PASSED;
}

TestResult TestOrGate()
{
    uint8_t test_failed = 0;
    uint8_t result;

    uint8_t x, y;

    x = y = 1;
    if( !(result = OrGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i OR %i returned %i\n", x, y, result);
    }

    x = 1;
    y = 0;
    if( !(result = OrGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i OR %i returned %i\n", x, y, result);
    }

    x = 0;
    y = 1;
    if( !(result = OrGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i OR %i returned %i\n", x, y, result);
    }

    x = y = 0;
    if( (result = OrGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i OR %i returned %i\n", x, y, result);
    }

    if( test_failed )
        return FAILED;

    return PASSED;
}

TestResult TestNotGate()
{
    uint8_t test_failed = 0;
    uint8_t result;

    uint8_t x;

    x = 1;
    if( (result = NotGate(x)) )
    {
        test_failed = 1;
        printf("Test failed: NOT %i returned %i\n", x, result);
    }

    x = 0;
    if( !(result = NotGate(x)) )
    {
        test_failed = 1;
        printf("Test failed: NOT %i returned %i\n", x, result);
    }

    if( test_failed )
        return FAILED;

    return PASSED;
}

TestResult TestXorGate()
{
    uint8_t test_failed = 0;
    uint8_t result;

    uint8_t x, y;

    x = y = 1;
    if( (result = XorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    x = 1;
    y = 0;
    if( !(result = XorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    x = 0;
    y = 1;
    if( !(result = XorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    x = y = 0;
    if( (result = XorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    if( test_failed )
        return FAILED;

    return PASSED;
}

TestResult TestXnorGate()
{
    uint8_t test_failed = 0;
    uint8_t result;

    uint8_t x, y;

    x = y = 1;
    if( !(result = XnorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    x = 1;
    y = 0;
    if( (result = XnorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    x = 0;
    y = 1;
    if( (result = XnorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    x = y = 0;
    if( !(result = XnorGate(x, y)) )
    {
        test_failed = 1;
        printf("Test failed: %i XOR %i returned %i\n", x, y, result);
    }

    if( test_failed )
        return FAILED;

    return PASSED;
}












TestResult RunTests()
{
    uint8_t result = PASSED;

    if(TestAndGate() == FAILED)
    {
        printf("TestAndGate failed!\n");
        result = FAILED;
    }
    else
    {
        printf("TestAndGate passed!\n");
    }

    if(TestOrGate() == FAILED)
    {
        printf("TestOrGate failed!\n");
        result = FAILED;
    }
    else
    {
        printf("TestOrGate passed!\n");
    }

    if(TestXorGate() == FAILED)
    {
        printf("TestXorGate failed!\n");
        result = FAILED;
    }
    else
    {
        printf("TestXorGate passed!\n");
    }

    if(TestNotGate() == FAILED)
    {
        printf("TestNotGate failed!\n");
        result = FAILED;
    }
    else
    {
        printf("TestNotGate passed!\n");
    }

    if(TestXnorGate() == FAILED)
    {
        printf("TestXnorGate failed!\n");
        result = FAILED;
    }
    else
    {
        printf("TestXnorGate passed!\n");
    }

    return result;
}
