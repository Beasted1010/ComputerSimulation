

#ifndef TEST_ENVIRONMENT_H
#define TEST_ENVIRONMENT_H

#include "gates.h"
#include "common.h"

typedef enum TestResult {
    PASSED,
    FAILED,
    ERROR_MISC
} TestResult;



TestResult TestAndGate();
TestResult TestOrGate();
TestResult TestNotGate();
TestResult TestXorGate();
TestResult TestXnorGate();

TestResult RunTests();

#endif
