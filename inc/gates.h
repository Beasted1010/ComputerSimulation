#ifndef GATES_H
#define GATES_H

#include "common.h"




Bit AndGate(Bit x, Bit y);
Bit OrGate(Bit x, Bit y);
Bit NotGate(Bit x);
Bit XorGate(Bit x, Bit y);

Bit XnorGate(Bit x, Bit y);
Bit NandGate(Bit x, Bit y);

#endif
