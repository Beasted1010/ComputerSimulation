

// I am sucking up the repeated inclusion of file_helpers and using the include guards for the sake of completeness
#include "stdio.h"
#include "stdlib.h"

#include "file_helpers.h"
#include "output_file.h"

static const char* in_file_name = "..\\files\\test_input_file.txt";
static const char* in_file_perm = "r+";

static const char* out_file_name = "..\\files\\test_output_file.txt";
static const char* out_file_perm = "w+";

int main(int argc, char** argv)
{
    FileInfo* in_finfo = malloc(sizeof(FileInfo));

    if(!in_finfo)
    {
        printf("Failed to allocate memory for in_finfo!\n");
        exit(1);
    }

    FillFileInfo(in_finfo, 0, in_file_name, in_file_perm);

    FileInfo* out_finfo = malloc(sizeof(FileInfo));

    if(!out_finfo)
    {
        printf("Failed to allocate memory for out_finfo!\n");
        exit(1);
    }

    FillFileInfo(out_finfo, 0, out_file_name, out_file_perm);


    #ifdef DELETE_LINES
        int num_lines = 6;
        int* lines = malloc(sizeof(int) * num_lines);
        for(int i =0; i < num_lines; i++)
        {
            lines[i] = i+1;
        }
        lines[0] = 1; // First ("TEST")
        lines[1] = 10; // Second to last ("JK")

        DeleteLines(in_fp, out_fp, lines, num_lines);

        free(lines);
    #endif

    //fseek(in_fp, 26, SEEK_SET);

    //DeleteRestOfFile(fileInfo);

    //EmptyFile(out_fp, out_file_name);

    DestroyFileInfo(in_finfo);
    DestroyFileInfo(out_finfo);

    return 0;
}
