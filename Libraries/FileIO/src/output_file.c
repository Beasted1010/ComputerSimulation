
#include "output_file.h"
#include "stdlib.h"
#include "string.h"

static inline int FileExists(FILE* fp)
{
    if(!fp)
    {
        printf("ERROR: The file has not yet been created!\n");
        return 0;
    }

    return 1;
}

ReturnCode DeleteLines(FileInfo* fileInfo, int* lines, int num_lines)
{
    if(!FileExists(fileInfo->file_pointer))
    {
        return ERROR_0;
    }

    // Go to the beginning of the file so that we may scan a line at a time for our desired line to delete
    if(fseek(fileInfo->file_pointer, 0, SEEK_SET))
    {
        printf("ERROR: Seeking to the beginning of the file failed!\n");
        return ERROR_1;
    }



    int warning_flag = 0; // Indicates if we encountered any warnings

    char ch = 0; // Initialize to some non-negative value (EOF should be some non-negative value)
    int line_count = 1; // Our current line number
    int lines_to_delete = num_lines;
    int delete_flag; // Flag to indicate that the line is to be deleted
    while(ch != EOF)
    {
        delete_flag = 0;
        ch = getc(fileInfo->file_pointer);

        if(ch == EOF)
        {
            break;
        }

        // Check to see if our current line is any of the lines we want to delete
        for(int i = 0; i < num_lines; i++)
        {
            if(line_count == lines[i])
            {
                delete_flag = 1;
            }
        }

        if(!delete_flag)
        {
            // TODO: putc(ch, out_fp);
        }
        else if(ch == '\n')
        {
            lines_to_delete--;

            if( lines_to_delete < 0 )
            {
                printf("WARNING: An extra line was just deleted! It was line %i.\n", line_count);
                warning_flag = 1;
            }
        }

        if(ch == '\n')
        {
            line_count++;
        }
    }

    if(lines_to_delete != 0)
    {
        printf("ERROR: Not all the requested lines were deleted! %i lines were missed!\n", lines_to_delete);
        return ERROR_2;
    }

    if(warning_flag)
    {
        return WARNING_0;
    }

    return SUCCESS;
}

/*ReturnCode DeleteRestOfLine(FILE* in_fp, FILE* out_fp)
{

    return SUCCESS;
}*/

// Returns the first portion of a filename (not the extension)
ReturnCode GetFilenamePrefix(char* filename, char** prefix)
{
    int warning = 0;

    if(!filename)
    {
        printf("ERROR: Filename is NULL!\n");
        printf("Exiting...\n");
        return ERROR_0;
    }

    if(!(*prefix))
    {
        printf("WARNING: Memory was not allocated for prefix!\n");
        printf("WARNING: Allocating the memory without call to free.\n");
        warning = 1;

        free(*prefix);
        *prefix = malloc(sizeof(char));

        if(!(*prefix))
        {
            printf("ERROR: Failed to allocate memory for the prefix!\n");
            return ERROR_0;
        }
    }

    int length = strlen(filename);

    for(int i = 0; i < length; i++)
    {
        if(filename[i] == '.')
        {
            break;
        }

        // Always ensure we have at least the length of what we will be accessing
        *prefix = realloc((*prefix), sizeof(char) * (i+1));

        if(!(*prefix))
        {
            printf("ERROR: Failed to reallocate memory for the prefix!\n");
            return ERROR_0;
        }

        (*prefix)[i] = filename[i];
    }

    if(warning)
    {
        return WARNING_0;
    }

    return SUCCESS;
}

// Function that basically gets the file extension
// TODO: Do I want a separate function to get the file extension which calls this...? Or just rename this function?
ReturnCode GetFilenameSuffix(char* filename, char** suffix)
{
    int warning = 0;

    if(!filename)
    {
        printf("ERROR: Filename is NULL!\n");
        printf("Exiting...\n");
        return ERROR_0;
    }

    // If the user didn't allocate the memory for suffix themselves, then we will allocate the memory but we won't free
    // This is a warning because the program can continue running but the memory will likely be leaked
    if(!(*suffix))
    {
        printf("WARNING: Memory was not allocated for suffix!\n");
        printf("WARNING: Allocating the memory without call to free.\n");
        warning = 1;

        free(*suffix);
        *suffix = malloc(sizeof(char));

        if(!(*suffix))
        {
            printf("ERROR: Failed to allocate memory for the suffix!\n");
            return ERROR_0;
        }
    }

    int length = strlen(filename);

    // Iterate over the entire filename
    for(int i = 0; i < length; i++)
    {
        // Have we come across the filename delimiter?
        if(filename[i] == '.')
        {
            // Start at the next character, iterate over the remaining characters
            // The remaining characters are apart of our suffix
            for(int j = (i+1); j < length+1; j++)
            {
                // Always ensure we have at least the length of what we will be accessing
                *suffix = realloc((*suffix), sizeof(char) * (j-i));

                if(!(*suffix))
                {
                    printf("ERROR: Failed to reallocate memory for the suffix!\n");
                    return ERROR_0;
                }

                (*suffix)[j-i-1] = filename[j];
            }
            break;
        }
    }

    if(warning)
    {
        return WARNING_0;
    }

    return SUCCESS;
}

ReturnCode CreateReplicaFile(FileInfo* fileInfo, char* replicaName, char** fileReplica)
{
    int warning = 0;

    if(strcmp(fileInfo->filename, replicaName) == 0)
    {
        printf("WARNING: The new file name can't be the same as the original copy!\n");
        warning = 1;

        const char* name_extension = "_copy";
        printf("Concatenating %s to file name's prefix!\n", name_extension);

        // Prefix will later grow into the full new filename, so allocate the size necessary now
        char* prefix = malloc(sizeof(char) * (strlen(replicaName) + strlen(name_extension)));
        if(!prefix)
        {
            printf("ERROR: Failed to allocate memory for the filename prefix!\n");
            return ERROR_0;
        }
        GetFilenamePrefix(fileInfo->filename, &prefix);

        char* suffix = malloc(sizeof(char));
        if(!suffix)
        {
            printf("ERROR: Failed to allocate memory for the filename prefix!\n");
            return ERROR_0;
        }
        GetFilenameSuffix(fileInfo->filename, &suffix);

        replicaName = realloc(replicaName, sizeof(char) * (strlen(replicaName) + strlen(name_extension)));

        if(!replicaName)
        {
            printf("ERROR: Failed to reallocate memory for the replica name!\n");
            return ERROR_0;
        }

        strcat(prefix, name_extension);
        strcat(prefix, ".");
        strcat(prefix, suffix);
        strcpy(replicaName, prefix);

        free(prefix);
        free(suffix);
    }

    // The variable to accumulate the result
    char* new_file_path = malloc(sizeof(char) * (fileInfo->absolute_directory_path_length + strlen(replicaName)));

    if(!new_file_path)
    {
        printf("ERROR: Failed to allocate memory for the new file path!\n");
        return ERROR_0;
    }

    strcpy(new_file_path, fileInfo->absolute_directory_path);
    strcat(new_file_path, replicaName);

    //char* file_copy_path = malloc(sizeof(char) * strlen(new_file_path));

    // Ensure we have enough memory for the replica file
    (*fileReplica) = realloc((*fileReplica), sizeof(char) * strlen(new_file_path));

    if(!(*fileReplica))
    {
        printf("ERROR: Failed to reallocate memory for file replica!\n");
        return ERROR_0;
    }

    strcpy((*fileReplica), new_file_path);

    free(new_file_path);

    if(warning)
    {
        return WARNING_0;
    }

    return SUCCESS;
}

/*
if(strcmp(inFileInfo->filename, new_file_name) == 0)
{
    warning_flag = 1;
    printf("WARNING The new file name can't be the same as the original copy!\n");

    const char* name_extension = "_copy";
    printf("Concatenating %s to file name!\n", name_extension);

    new_file_name = realloc(new_file_name, sizeof(char) * strlen(new_file_name) + sizeof(name_extension));
    strcat(new_file_name, name_extension);
}
*/

ReturnCode DeleteRestOfFile(FileInfo* fileInfo)
{
    if(!FileExists(fileInfo->file_pointer))
    {
        return ERROR_0;
    }

    char* replica_name = malloc(sizeof(char) * 15);

    if(!replica_name)
    {
        printf("ERROR: Failed to allocate memory for the replica name!\n");
        return ERROR_0;
    }

    strcpy(replica_name, "replica.txt");
    char* temp_filename = malloc(sizeof(char));

    if(!temp_filename)
    {
        printf("ERROR: Failed to allocate memory for the temporary filename!\n");
        return ERROR_0;
    }

    CreateReplicaFile(fileInfo, replica_name, &temp_filename);

    FILE* temp_fp = fopen(temp_filename, "w+");

    if(!FileExists(temp_fp))
    {
        return ERROR_0;
    }

    // We wish to remain unobstructive and preserve the callers current file position
    fpos_t pos_save;
    fgetpos(fileInfo->file_pointer, &pos_save);

    long int stop_point = ftell(fileInfo->file_pointer);

    // Start at the beginning of the file
    fseek(fileInfo->file_pointer, 0, SEEK_SET);

    char ch;// getc(fileInfo->file_pointer); // Eat up the dangling new line
    for(int curr_pos = 0; curr_pos < stop_point; curr_pos++)
    {
        ch = getc(fileInfo->file_pointer);

        // New lines seem to be skipped over in ftell, so compensate by backing up curr_pos one spot
        if(ch == '\n')
        {
            curr_pos--;
        }

        if(ch != EOF)
        {
            putc(ch, temp_fp);
        }
    }

    // Close both files
    fclose(fileInfo->file_pointer);
    fclose(temp_fp);

    // Remove the file we had, we want to ultimately replace fileInfo->file_pointer with temp_fp
    remove(fileInfo->absolute_file_path);

    // Now rename the new file to what the old file was called, effectively resulting in no new files in user's eyes
    rename(temp_filename, fileInfo->absolute_file_path);

    fileInfo->file_pointer = fopen(fileInfo->absolute_file_path, fileInfo->file_permissions);

    if(!fileInfo->file_pointer)
    {
        printf("ERROR: File was unable to be opened after rename!\n");
        return ERROR_1;
    }

    fsetpos(fileInfo->file_pointer, &pos_save); // TODO: Will this still work?

    free(temp_filename);
    free(replica_name);

    return SUCCESS;
}
