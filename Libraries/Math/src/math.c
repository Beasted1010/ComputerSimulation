
#include "math.h"

float Exponentiate( float base, float exponent )
{
    int i;
    float result = 1;
    for( i = 0; i < exponent; i++ )
    { result *= base; }

    return result;
}

float DecimalExponentExponentiate( float base, float exponent )
{
    float result = 1;
    // TODO: Need to figure out way to calculate an irrational exponent
    return result;
}

float Square( float base )
{
    return Exponentiate( base, 2 );
}

float PerToDec( float rateInPer )
{
    return rateInPer / 100;
}
