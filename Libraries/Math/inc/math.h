
#ifndef MY_MATH_H
#define MY_MATH_H

//CONSTANTS
static const float PI = 3.1415926535f;
static const float e = 2.718281828f;

float Exponentiate( float base, float exponent );
float DecimalExponentExponentiate( float base, float exponent );
float Square( float base );
float PerToDec( float rateInPer );


#endif /* math.h */
