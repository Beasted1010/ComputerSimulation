
@echo off

cd obj

gcc -c -I ../inc ../src/math.c

cd ../lib

ar rcs libmath.a ../obj/math.o

cd ..
