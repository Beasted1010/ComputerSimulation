

@echo off

cd AutoGen/obj

gcc -c -I ../inc -I ../../Libraries/Math/inc -I ../../Libraries/FileIO/inc -I ../../inc ../src/main.c

cd ../bin

gcc -o run_auto_gen -L ../../Libraries/Math/lib -L ../../Libraries/FileIO/lib ../obj/main.o -lmath -lfile_io

cd ../..
