
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "math.h"
#include "file_helpers.h"

// TODO: ERROR HANDLING FOR FAILED MALLOC CALLS

static const char* in_file_name = "..\\io\\input_files\\input.txt";
static const char* in_file_perm = "r+";

static const char* out_file_name = "..\\io\\output_files\\output.txt";
static const char* out_file_perm = "w";

void GenerateTableComment(FILE* fp, char* function);

int main(int argc, char** argv)
{
    FileInfo* in_finfo = malloc(sizeof(FileInfo));

    if(!in_finfo)
    {
        printf("Failed to allocate memory for in_finfo!\n");
        exit(1);
    }

    FillFileInfo(in_finfo, 0, in_file_name, in_file_perm);

    FileInfo* out_finfo = malloc(sizeof(FileInfo));

    if(!out_finfo)
    {
        printf("Failed to allocate memory for out_finfo!\n");
        exit(1);
    }

    FillFileInfo(out_finfo, 0, out_file_name, out_file_perm);


    GenerateTableComment(out_finfo->file_pointer, "a * b * b * b * a * ~z + b * c * c + a");

    DestroyFileInfo(in_finfo);
    DestroyFileInfo(out_finfo);

    return 0;
}


typedef enum TruthValues
{
    False, // 0
    True   // 1
} TruthValues;


typedef struct TruthTable
{
    int num_rows;
    int num_cols;
    int num_variables;
    char* variables;
    TruthValues** truthValueTable;

    char* function;
    TruthValues* functionOutput;
} TruthTable;

// The boolean variables we accept
const char* BOOLEAN_VARIABLES = "abcdefghijklmnopqrstuvwxyz";

// The boolean operators we accept
// TODO: ADD MORE (XOR, XAND, NAND, NOR, etc.) -> Whatever symbols we can represent...
const char* BOOLEAN_OPERATORS = "~,+|,*,";

typedef enum BooleanOperators
{
    NOT = '~',
    OR = '+',
    OR_BAR = '|',
    AND = '*'
} BooleanOperators;


//--------------------------------------------------Token Matching--------------------------------------------------
int IsVariable(char toTest)
{
    // Is our value in the list of recognized boolean variables?
    for(int i = 0; i < strlen(BOOLEAN_VARIABLES); i++)
    {
        if(toTest == BOOLEAN_VARIABLES[i])
        {
            return 1;
        }
    }
    return 0;
}

int IsOperator(char toTest)
{
    // Is our value in the list of recognized boolean operators?
    for(int i = 0; i < strlen(BOOLEAN_OPERATORS); i++)
    {
        // Comma is our delimiter, don't consider it
        if(BOOLEAN_OPERATORS[i] != ',' && toTest == BOOLEAN_OPERATORS[i])
        {
            return 1;
        }
    }
    return 0;
}

int IsInLanguage(char toTest)
{
    if(IsVariable(toTest) || IsOperator(toTest))
    {
        return 1;
    }

    return 0;
}
//--------------------------------------------------END Token Matching--------------------------------------------------

int DoBinaryBooleanOperation(char operator, int operand1, int operand2)
{
    switch(operator)
    {
        case OR_BAR:
        case OR:
        {
            return (operand1 || operand2);
        } break;

        case AND:
        {
            return (operand1 && operand2);
        } break;

        default:
        {
            printf("The specified operator is not supported!\n");
            return -1;
        } break;
    }
}

void PrintTruthTableToFile(TruthTable* table, FILE* filePointer)
{
    // Used to center the function output under the output column
    int functionOutputOffset = strlen(table->function) / 2;

    // Print out each of the variables that we have -> Only one row of them
    for(int i = 0; i < table->num_variables; i++)
    {
        fprintf(filePointer, "%c\t", table->variables[i]);
    }

    // Tack on the function
    fprintf(filePointer, "%s\n", table->function);

    // Iterate over each row in our table
    for(int i = 0; i < table->num_rows; i++)
    {
        // For each row, print out each of the truth values for our table
        for(int j = 0; j < table->num_cols - 1; j++)
        {
            fprintf(filePointer, "%i\t", table->truthValueTable[i][j]);
        }

        // Make the table look pretty by having the output column centered under the function
        for(int j = 0; j < functionOutputOffset; j++)
        {
            fprintf(filePointer, " ");
        }

        // End the line with the output of the function
        fprintf(filePointer, "%i\n", table->functionOutput[i]);
    }
}

void PrintTruthTable(TruthTable* table)
{
    // Used to center the function output under the output column
    int functionOutputOffset = strlen(table->function) / 2;

    // Print out each of the variables that we have -> Only one row of them
    for(int i = 0; i < table->num_variables; i++)
    {
        printf("%c\t", table->variables[i]);
    }

    // Tack on the function
    printf("%s\n", table->function);

    // Iterate over each row in our table
    for(int i = 0; i < table->num_rows; i++)
    {
        // For each row, print out each of the truth values for our table
        for(int j = 0; j < table->num_cols - 1; j++)
        {
            printf("%i\t", table->truthValueTable[i][j]);
        }

        // Make the table look pretty by having the output column centered under the function
        for(int j = 0; j < functionOutputOffset; j++)
        {
            printf(" ");
        }

        // End the line with the output of the function
        printf("%i\n", table->functionOutput[i]);
    }
}

// Just populate the default table of truth values
void PopulateDefaultTableValues(TruthTable* table)
{
    // num_rows should of been a power of 2, so if it is not divisible by 2, then there was an error in it's calculation
    if(table->num_rows % 2 != 0)
    {
        printf("WARNING: The table's number of rows was incorrectly calculated (not a power of 2)!\n");
    }

    // The half point is the point at which we will go from outputting 1s to outputting 0s
    int halfPoint = table->num_rows / 2.0;

    // The half point tracker will be used to keep track of when we should reset to outputting the opposite value
    int halfPointTracker = halfPoint;
    int truthValue = 1; // Start with 1s (truthy) first

    // Don't include the function output part of table
    for(int i = 0; i < table->num_cols - 1; i++)
    {
        // If we start a new iteration with a half point that is <= 0 then we are in trouble...
        if(halfPoint <= 0)
        {
            printf("ERROR: The half point was incorrectly created, calculated, or handled! Value is 0 or below!\n");
            exit(1);
        }

        for(int j = 0; j < table->num_rows; j++)
        {
            table->truthValueTable[j][i] = truthValue;

            // We have just added a value to the colum, now update how close we are to the next half point
            halfPointTracker--;

            // Have we used up all of the current truth value for this section?
            if(halfPointTracker <= 0)
            {
                // Swap the truth value as we progress down the next section
                truthValue = !truthValue;

                // Reset the half point tracker so that we have the same number of iterations next round
                halfPointTracker = halfPoint;
            }
        }

        // Each column over is has double the frequency of reaching halfpoints, adjust the halfPoint value to show this
        halfPointTracker = halfPoint /= 2.0;
    }
}

int GetNumberOfUniqueVariables(char* function, char** unique_variables_out)
{
    char* recognizedVariables = malloc(sizeof(char));
    int numRecognizedVariables = 0;

    int unique = 1;

    for(int i = 0; i < strlen(function); i++)
    {
        if(IsVariable(function[i]))
        {
            // Always start by assuming uniqueness
            unique = 1;

            // Check if the variable has already been counted for
            // If we have 0 recognized variables, it is unique by default (for loop won't execute)
            for(int j = 0; j < numRecognizedVariables; j++)
            {
                // If our current variable is a recognized variables, then it is not unique (already been counted for)
                if(function[i] == (*unique_variables_out)[j])
                {
                    unique = 0;
                }
            }

            if(unique)
            {
                (*unique_variables_out)[numRecognizedVariables++] = function[i];
                (*unique_variables_out) = realloc((*unique_variables_out), sizeof(char) * (numRecognizedVariables+1));
            }
        }
    }

    return numRecognizedVariables;
}

int GrabTableValue(TruthTable* truthTable, int current_row, char variable)
{
    for(int i = 0; i < truthTable->num_cols - 1; i++)
    {
        if(truthTable->variables[i] == variable)
        {
            return truthTable->truthValueTable[current_row][i];
        }
    }
}

void CondenseIntArray(int** array, int* num_operand_bits, int new_value_index, int new_value)
{
    // Add the new value to the new value index in the array
    (*array)[new_value_index] = new_value;

    // Ignore the last operand bit, it will be pulled in and that spot will be discarded
    // Update the index we will be using so that we start 1 ahead of the updated value
    for(int i = ++new_value_index; i < (*num_operand_bits) - 1; i++)
    {
        // From here till one less than the end, each spot in array is replaced with the next spot
        (*array)[i] = (*array)[i+1];
    }

    // Reduce array size by one (update number of operand bits)
    (*array) = realloc((*array), sizeof(int) * (--(*num_operand_bits)));
}

void SeparateOperandsAndOperators(char* function, char** operands, char** operators)
{
    int operandIndex = 0;
    int operatorIndex = 0;

    // Iterate over function and separate the function operands from the function operators
    for(int i = 0; i < strlen(function); i++)
    {
        if(IsVariable(function[i]))
        {
            (*operands)[operandIndex++] = function[i];
            (*operands) = realloc((*operands), sizeof(char) * (operandIndex + 1));
        }
        else if(IsOperator(function[i]))
        {
            (*operators)[operatorIndex++] = function[i];
            (*operators) = realloc((*operators), sizeof(char) * (operatorIndex + 1));
        }
    }

    // Treat these character arrays as strings so that we may find their length easily without a separate variable
    (*operands)[operandIndex] = '\0';
    (*operators)[operatorIndex] = '\0';
}

// Convert an array of variables to an array of bits (1s and 0s)
void VariableToBits(int* resultBits, char* variables, int num_unique_var, char* variables_used, int* var_to_bit_mapping)
{
    // The result will be the same length as our variable array, but just convereted to 1s and 0s
    //int* result = malloc(sizeof(char) * (strlen(variables)));

    for(int i = 0; i < strlen(variables); i++)
    {
        // Find which variable we are on, provide the corresponding mapped bit to the result
        for(int j = 0; j < num_unique_var; j++)
        {
            if(variables[i] == variables_used[j])
            {
                resultBits[i] = var_to_bit_mapping[j];
                break;
            }
        }
    }
}

// Quest: Compute the function output value given corresponding bit values for each variable used
int ComputeFunctionValue(char* function, int num_unique_var, char* variables_used, int* var_to_bit_mapping)
{
    char* operands = malloc(sizeof(char));
    char* operators = malloc(sizeof(char));

    int resultValue;

    // Start by separating our operands and operators
    SeparateOperandsAndOperators(function, &operands, &operators);

    int* operandBits = malloc(sizeof(int) * strlen(operands));

    VariableToBits(operandBits, operands, num_unique_var, variables_used, var_to_bit_mapping);

    int currentOperandIndex = 0;
    int numOperandBits = strlen(operands);

    // Handle each operator one at a time
    for(int i = 0; i < strlen(operators); i++)
    {
        if(operators[i] == NOT)
        {
            operandBits[currentOperandIndex] = !operandBits[currentOperandIndex];
        }
        else
        {
            resultValue = DoBinaryBooleanOperation(operators[i], operandBits[currentOperandIndex],
                                                                 operandBits[currentOperandIndex + 1]);

            if(resultValue < 0)
            {
                printf("ERROR: Operator %s is invalid for operands %i and %i\n", operators[i],
                                                                                 operandBits[currentOperandIndex],
                                                                                 operandBits[currentOperandIndex + 1]);
                exit(1);
            }

            if(currentOperandIndex > strlen(operands))
            {
                printf("The operandIndex is greater than the number of possible operands!... Crashing!\n");
                exit(1);
            }

            CondenseIntArray(&operandBits, &numOperandBits, (currentOperandIndex), resultValue);
        }
    }

    free(operands);
    free(operators);
    free(operandBits);

    return resultValue;
}

// Quest: Populate the functionOutput field in the TruthTable structure
// Given: A function and the default values for a truth table
// Strategy: Step1: Find all variables used and their corresponding boolean values. Step2: Use these in computation
// Assumption: Infix application of operator
void SolveTruthTableFunction(TruthTable* truthTable)
{
    // Allocate the memory for num_rows many function output values
    truthTable->functionOutput = malloc(sizeof(TruthValues) * truthTable->num_rows);

    char* variables_used = malloc(sizeof(char));
    int num_unique_var = GetNumberOfUniqueVariables(truthTable->function, &variables_used);
    char* var_operands = malloc(sizeof(char) * num_unique_var);
    int* var_to_bit_mapping = malloc(sizeof(int) * num_unique_var);


    int* bit_operands = malloc(sizeof(int));
    int operandIndex = 0;
    char operator;

    int result = -1;

    // For each row, grab the corresponding table values used by our function and compute the output
    for(int j = 0; j < truthTable->num_rows; j++)
    {
        // For this row, find all the corresponding bit values for each variable
        for(int k = 0; k < num_unique_var; k++)
        {
            // Given the row and the variable (column), find the corresponding bit value
            var_to_bit_mapping[k] = GrabTableValue(truthTable, j, variables_used[k]);
        }

        // At this point: We have a mapping from the variables in our function to 1s and 0s
        // Now: Compute the value for this row
        truthTable->functionOutput[j] = ComputeFunctionValue(truthTable->function, num_unique_var,
                                                                variables_used, var_to_bit_mapping);
    }
}

char* RemoveWhiteSpace(char* str)
{
    char* result = malloc(sizeof(char));
    int resultIndex = 0;

    // Copy the string over, include the null terminating character
    for(int i = 0; i < strlen(str) + 1; i++)
    {
        if(str[i] != ' ')
        {
            result[resultIndex++] = str[i];
            result = realloc(result, sizeof(char) * (resultIndex+1));
        }
    }

    return result;
}

int CountNumberOfVariables(char* function)
{
    int numberOfVariables = 0;

    for(int i = 0; i < strlen(function); i++)
    {
        if(IsVariable(function[i]))
        {
            numberOfVariables++;
        }
    }

    return numberOfVariables;
}

TruthTable* CreateTruthTable(char* function)
{
    printf("Creating the truth table...\n");
    TruthTable* truthTable = malloc(sizeof(TruthTable));

    if(!truthTable)
    {
        printf("ERROR: Failed to allocate memory for the truth table structure!\n");
        exit(1);
    }

    truthTable->function = malloc(sizeof(char) * strlen(function));

    if(!truthTable->function)
    {
        printf("ERROR: Failed to allocated memory for the function!\n");
        exit(1);
    }

    // TODO: Ensure valid function (valid tokens are used)

    // Get a version of our function that includes no whitespace
    char* bareFunction = RemoveWhiteSpace(function);

    truthTable->function = bareFunction;

    truthTable->variables = malloc(sizeof(char) * truthTable->num_variables);

    if(!truthTable->variables)
    {
        printf("ERROR: Failed to allocate memory for the truth table variables!\n");
        exit(1);
    }

    truthTable->num_variables = GetNumberOfUniqueVariables(truthTable->function, &truthTable->variables);;

    if(truthTable->num_variables > strlen(BOOLEAN_VARIABLES))
    {
        printf("ERROR: Need more boolean variables! We have %i, and truth table asks for %i\n",
                                                strlen(BOOLEAN_VARIABLES), truthTable->num_variables);
        exit(1);
    }

    truthTable->num_rows = Exponentiate(2, truthTable->num_variables);
    truthTable->num_cols = truthTable->num_variables + 1; // Include output column

    // Allocate the space for the pointers to our truth values. There are num_rows many.
    truthTable->truthValueTable = malloc(sizeof(TruthValues*) * (truthTable->num_rows));

    if(!truthTable->truthValueTable)
    {
        printf("ERROR: Failed to allocate memory for the truth value table rows!\n");
        exit(1);
    }

    // For each row in our truth table, allocate the memory for that row (for each column) -> Exclude output column
    for(int i = 0; i < truthTable->num_rows; i++)
    {
        truthTable->truthValueTable[i] = malloc(sizeof(TruthValues) * (truthTable->num_cols - 1));

        if(!(truthTable->truthValueTable)[i])
        {
            printf("ERROR: Failed to allocate memory for the truth value table for row %i\n", i+1);
            exit(1);
        }
    }

    PopulateDefaultTableValues(truthTable);


    // TODO: IMPLEMENT OPERATOR PRECEDENCE
    SolveTruthTableFunction(truthTable);




    PrintTruthTable(truthTable);

    printf("Truth table created!\n");

    return truthTable;
}

void DestroyTruthTable(TruthTable* truthTable)
{
    printf("Destroying the truth table...\n");

    free(truthTable->variables);

    // Free each row in our truth value table
    for(int i = 0; i < truthTable->num_rows; i++)
    {
        free((truthTable->truthValueTable)[i]);
    }

    // Free the pointer to a row
    free(truthTable->truthValueTable);

    free(truthTable->function);
    free(truthTable->functionOutput);

    printf("Truth table destroyed!\n");
}

void GenerateTableComment(FILE* fp, char* function)
{

    TruthTable* truthTable = CreateTruthTable(function);

    PrintTruthTableToFile(truthTable, fp);

    // TODO: Create comment stuff here

    DestroyTruthTable(truthTable);
}
