

// I am sucking up the repeated inclusion of file_helpers and using the include guards for the sake of completeness
#include "stdio.h"
#include "stdlib.h"

#include "file_helpers.h"
#include "output_file.h"

// TODO Perhaps have a file structure that contains this information? Why not use the file structure in file_helpers.h?
static const char* in_file_name = "..\\io\\input_files\\input.txt";
static const char* in_file_perm = "r+";

static const char* out_file_name = "..\\io\\output_files\\output.txt";
static const char* out_file_perm = "a";

int main(int argc, char** argv)
{
    printf("Opening files...\n");
    FILE* in_fp = fopen(in_file_name, in_file_perm);

    if(!in_fp)
    {
        printf("File %s was unable to be opened with permissions %s\n", in_file_name, in_file_perm);
        exit(1);
    }

    FILE* out_fp = fopen(out_file_name, out_file_perm);

    if(!out_fp)
    {
        printf("File %s was unable to be opened with permissions %s\n", out_file_name, out_file_perm);
        exit(1);
    }

    int num_lines = 6;
    int* lines = malloc(sizeof(int) * num_lines);
    for(int i =0; i < num_lines; i++)
    {
        lines[i] = i+1;
    }
    //lines[0] = 1; // First ("TEST")
    //lines[1] = 10; // Second to last ("JK")

    //DeleteLines(in_fp, out_fp, lines, num_lines);

    FileInfo* fileInfo = malloc(sizeof(FileInfo));

    FillFileInfo(fileInfo, in_fp, in_file_name, in_file_perm);

    //fseek(in_fp, 12, SEEK_SET);

    //DeleteRestOfFile(fileInfo);

    //EmptyFile(out_fp, out_file_name);

    DestroyFileInfo(fileInfo);

    printf("Closing files...\n");
    fclose(in_fp);
    fclose(out_fp);

    return 0;
}


// gcc -I ../../Libraries/FileIO/inc -L ../../Libraries/FileIO/lib test.c -lfile_io
